#!/usr/bin/env node

const Mopidy = require("mopidy");
const Config = require("./config");
const gpio = require("rpi-gpio");

const mopidy = new Mopidy({ 
    autoConnect: false,
    webSocketUrl: Config.webSocketUrl
});

// Handle all GPIO input in this watcher
gpio.on('change', function(channel, value) {
	console.log('Channel ' + channel + ' value is now ' + value);
});

// Setup buttons (inputs)
let buttons = Config.gpioLayout.buttons;
for(key in buttons) {
    gpio.setup(buttons[key], gpio.DIR_IN, gpio.EDGE_BOTH);
}

// Setup leds (outputs)
let leds = Config.gpioLayout.leds;
for(key in leds) {
    gpio.setup(leds[key], gpio.DIR_OUT, gpio.EDGE_BOTH);
}

function err()
{
    // Light up the red led
    gpio.write(Config.gpioLayout.leds.error, true);
}

// Just a bit of mopidy debug output
mopidy.on("state", console.log);
mopidy.on("event", console.log);

// Connect to mopidy
mopidy.connect();

// Light up the green led when everything is ready
gpio.write(Config.gpioLayout.leds.ready, true);
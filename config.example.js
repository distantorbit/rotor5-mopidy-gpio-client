module.exports = {
    webSocketUrl: "ws://127.0.0.1:6680/mopidy/ws/",
    gpioLayout: {
        buttons: {
            previous: 19,  // GPIO09
            playPause: 21, // GPIO10
            next: 23,      // GPIO11
            volumeUp: 8,   // GPIO14
            volumeDown: 10 // GPIO15
        },
        leds: {
            ready: 16,     // GPIO23
            error: 18      // GPIO24
        }
    }
}
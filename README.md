# README #

I'm building a music box for my kids. I want them to be able to control Spotify with physical buttons and not a tablet or phone.

The physical build consists of:

* An old speaker
* A Raspberry Pi 3
* A HifiBerry Amp2 - https://www.hifiberry.com/shop/boards/hifiberry-amp2/
* A Mopidy server running on the RasPi - https://mopidy.com/
* Some buttons mounted on the speaker, connected over GPIO

The functionality I want:

* Play/Pause (one button)
* Next/Previous track (two buttons)
* ~~Next playlist (one button)~~
* Next/Previous playlist (long press next/previous track)
* Volume up/down ~~(potentiometer)~~ (two buttons)
* A led that lights up when the server is booted and ready (may replace this with voice message)
* Text-to-speech that says the name of the playlist when switching playlists
* Shuffle (toggle with long press playlist button)

Dependencies:

* Nodejs - https://nodejs.org
* Mopidy.js - https://github.com/mopidy/mopidy.js
* rpi-gpio - https://github.com/JamesBarwell/rpi-gpio.js

### How do I get set up? ###

* Sync the repo
* `npm install`

### Contribution guidelines ###

I am not planning to collaborate with anyone on this project, it's obviouslu very specific for my particular build. This repository is public for two reasons. First it may serve as inspiration for others with similar projects. Second, I can sync this repo on my RasPi without messing with ssh keys, so I can develop on my main box and deploy on the Pi.